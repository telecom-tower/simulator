package main

//go:generate esc -prefix html -ignore .DS_Store -o html.go -pkg main html

import (
	"context"
	"flag"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"runtime"
	"sync"
	"time"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	renderer "gitlab.com/telecom-tower/grpc-renderer"
	ws2811 "gitlab.com/telecom-tower/web_ws281x"
)

var version = "master"

func startBrowser(url string) bool {
	// try to start the browser
	var args []string
	switch runtime.GOOS {
	case "darwin":
		args = []string{"open"}
	case "windows":
		args = []string{"cmd", "/c", "start"}
	default:
		args = []string{"xdg-open"}
	}
	cmd := exec.Command(args[0], append(args[1:], url)...) // nolint: gas
	return cmd.Start() == nil
}

func main() { // nolint: gocyclo
	debug := flag.Bool("debug", false, "Debug mode")
	verbose := flag.Bool("verbose", false, "Verbose mode")
	showVer := flag.Bool("version", false, "Show version")
	noStartBrowser := flag.Bool("no-start-browser", false, "Don't start browser")
	httpAddress := flag.String("http-address", "127.0.0.1", "listening HTTP address")
	httpPort := flag.Int("http-port", 8080, "listening HTTP port")
	grpcPort := flag.Int("grpc-port", 10000, "listening gRPC port")

	flag.Parse()

	if *showVer {
		fmt.Printf("Telecom Tower Simulator // version : %v\n", version)
		os.Exit(0)
	}

	if *debug {
		log.SetLevel(log.DebugLevel)
	} else if *verbose {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.WarnLevel)
	}

	// Create and run hub
	hub := ws2811.NewHub()
	wsopt := ws2811.DefaultOptions
	wsopt.Channels[0].LedCount = 1024
	ws, err := ws2811.MakeWS2811(&wsopt, hub)
	if err != nil {
		log.Fatal(err)
	}
	if err = ws.Init(); err != nil {
		log.Fatal(err)
	}

	var wg sync.WaitGroup

	wg.Add(1)
	go func() {
		err := hub.Serve()
		log.Info(err)
		wg.Done()
	}()

	r := mux.NewRouter()
	r.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		log.Debug("Serving websocket")
		ws2811.ServeWs(hub, w, r)
	})
	r.PathPrefix("/").Handler(http.FileServer(FS(false))) // nolint
	// r.PathPrefix("/").Handler(http.FileServer(http.Dir("html"))) // nolint

	grpcLis, err := net.Listen("tcp", fmt.Sprintf(":%d", *grpcPort))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	renderer := renderer.NewRenderer(ws)
	wg.Add(1)
	go func() {
		err := renderer.Serve(grpcLis)
		if err != nil {
			log.Error(err)
		}
		wg.Done()
	}()

	a := fmt.Sprintf("%s:%d", *httpAddress, *httpPort)
	srv := &http.Server{
		Handler: r,
		Addr:    a,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	wg.Add(1)
	go func() {
		err := srv.ListenAndServe()
		log.Info(err)
		wg.Done()
	}()

	log.Infof("HTTP server ready at http://%s", a)

	if !*noStartBrowser {
		if !startBrowser(fmt.Sprintf("http://%s", a)) {
			log.Warn("Unable to start browser")
		}
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	log.Info("Shuting down... please wait.")

	hub.Shutdown()
	renderer.Shutdown()
	log.Info("Renderer closed")

	err = srv.Shutdown(context.Background())
	if err != nil {
		log.Error(err)
	}
	log.Info("Finished")
	wg.Wait()
}
